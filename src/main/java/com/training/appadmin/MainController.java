package com.training.appadmin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class MainController {

    @Value("${text.greeting}")
    private String greeting;

    @RequestMapping("/greeting")
    public String getGreeting() {
        return greeting;
    }

}
